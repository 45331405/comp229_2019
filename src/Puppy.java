import java.awt.*;
public class Puppy implements Actors{
    Cell cell;
    public Puppy(Cell cell){
        this.cell = cell;
    }
    public void paint (Graphics g){
     
        g.setColor(Color.GREEN);
        g.fillRect(cell.x, cell.y, cell.height, cell.width);


    }
}