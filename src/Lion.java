import java.awt.*;
public class Lion implements Actors{
    Cell cell;
    public Lion(Cell cell){
        this.cell = cell;
    }
    public void paint (Graphics g){
     
        g.setColor(Color.RED);
        g.fillRect(cell.x, cell.y, cell.height, cell.width);


    }
}