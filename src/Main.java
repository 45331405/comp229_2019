import javax.swing.*;
import java.awt.*;

public class Main extends JFrame {

    Stage stage;
    
    public static void main(String[] args) throws Exception {
        Main window = new Main();
        window.run();
    }

    public class Canvas extends JPanel{

        public Canvas(){
            setPreferredSize(new Dimension(720,720));
            stage = new Stage();
        }

        @Override
        public void paint(Graphics g){

            stage.paint(g, getMousePosition());
        }

       
        
    }

    public Main(){
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Canvas canvas = new Canvas();
        this.setContentPane(canvas);
        this.pack();
        this.setVisible(true);
        
    }

    public void run(){
        while(true)
        {
            this.repaint();
        }
    }
}


