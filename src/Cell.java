import java.awt.*;

public class Cell extends Rectangle{

    static int size = 35;

    public Cell(int x , int y){
        
        super(x, y ,size,size);
        
    }

    
    public void paint (Graphics g, Point mousepos){
        if (isMouseIN(mousepos)) {
            g.setColor(Color.RED);
        }else{
            g.setColor(Color.WHITE);
        }
        g.fillRect(x, y, size, size);
        g.setColor(Color.BLACK);
        g.drawRect(x, y, size, size);

    }


    boolean isMouseIN( Point  p){

        if (p!= null) {
            return(x < p.x && x+size >p.x && y < p.y && y+size > p.y );
        }else{
            return false;
        }
    }
}